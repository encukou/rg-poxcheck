import base64
import struct

from regeneration.poxcheck import enums

def chunk(fmt, string):
    size = struct.calcsize(fmt)
    return string[size:], struct.unpack(fmt, string[:size])

def find_in(val, *maps):
    oval = val
    for map in maps:
        for ke, va in map.items():
            if oval == va:
                val = "%s:%s" % (val, ke)
                break
    return val

def analyze(lineString):
    try:
        a, sep, b64 = lineString.partition('$')
    except AttributeError:
        return
    if sep:
        decoded = base64.b64decode(b64)
        if len(decoded) < 15:
            print repr(decoded)
        if lineString.startswith('C'):
            for k, v in enums.Command.items():
                if v == ord(decoded[0]):
                    print '(Command %s)' % k
                    if k == 'EngageBattle':
                        # Battle ID, 0, Other battle ID
                        fmt = '<III'
                        # Configuration: gen, mode, id[2], clauses
                        fmt += 'BBIII'
                        # Pokemon: num, nick, maxhp, gender, shiny, level, item, ability happiness
                        #    stat[6], move[4], gene[6], eff[6]
                        unpacked = struct.unpack(
                                fmt,
                                decoded[:struct.calcsize(fmt)],
                            )
                        decoded = decoded[struct.calcsize(fmt)+1:]
                        bid, zero, obid = unpacked[0:3]
                        gen, mode, id1, id2, clauses = unpacked[3:8]
                        for x in 'bid zero obid gen mode id1 id2 clauses'. split():
                            print x, locals()[x]
                        for i in range(6):
                            pokenum, subnum, nick_len = struct.unpack('>HBi', decoded[:7])
                            if nick_len == -1:
                                nick_len = 0
                                nonick = True
                            else:
                                nonick = False
                            if nick_len > 50:
                                print repr(decoded)
                            decoded = decoded[7:]
                            nick = decoded[:nick_len]
                            if nonick:
                                nick = None
                            print nick_len
                            decoded = decoded[nick_len:]
                            pok = '>HHBBBHHB'
                            maxhp, curhp, gender, shiny, level, item, ability, tameness = struct.unpack(pok, decoded[:struct.calcsize(pok)])
                            decoded = decoded[struct.calcsize(pok):]
                            nstats = struct.unpack('>5H', decoded[:10])
                            decoded = decoded[10:]
                            moves = []
                            for j in range(4):
                                moves.append(struct.unpack('>HBB', decoded[:4]))
                                decoded = decoded[4:]
                            evs = struct.unpack('>6I', decoded[:24])
                            decoded = decoded[24:]
                            dvs = struct.unpack('>6I', decoded[:24])
                            decoded = decoded[24:]
                            print '--Pokemon', i
                            for x in 'pokenum subnum nick maxhp curhp gender shiny level item ability tameness nstats moves evs dvs'. split():
                                print x, locals()[x]
                    else:
                        continue
                    break
        elif lineString.startswith('B'):
            for k, v in enums.BattleCommand.items():
                if v == ord(decoded[0]):
                    print '(BattleCommand %s)' % k
                    if k == 'SendOut':
                        decoded, (side, slot) = chunk(">HH", decoded)
                        decoded, (pokenum, subnum, nick_len) = chunk(">HBi", decoded)
                        if nick_len == -1:
                            nick_len = 0
                            nonick = True
                        else:
                            nonick = False
                        print nick_len
                        nick = decoded[:nick_len]
                        if nonick:
                            nick = None
                        decoded = decoded[nick_len:]
                        decoded, (hpperc, fullstatus, gender, shiny, level) = chunk(">BIBBB", decoded)
                        for x in 'side slot pokenum subnum nick hpperc fullstatus gender shiny level'. split():
                            print x, locals()[x]
                    elif k == 'OfferChoice':
                        decoded, (n, slot1, slot2, switch, attacks, a1, a2, a3, a4) = chunk(">BBB??????", decoded)
                        for x in 'slot1 slot2 switch attacks a1 a2 a3 a4'. split():
                            print x, locals()[x]
                    elif k == 'ChangeHp':
                        decoded, (n, player, hp) = chunk(">BBH", decoded)
                        for x in 'player hp'. split():
                            print x, locals()[x]
                    elif k == 'ChangePP':
                        decoded, (n, player, move, pp) = chunk(">BBBB", decoded)
                        for x in 'player move pp'. split():
                            print x, locals()[x]
                    elif k == 'StatusChange':
                        decoded, (n, player, status, a, b) = chunk(">BBBBB", decoded)
                        for ke, va in enums.Status.items():
                            if status == va:
                                status = "%s:%s" % (status, ke)
                                break
                        for x in 'player status a b'. split():
                            print x, locals()[x]
                    elif k == 'StatusMessage':
                        decoded, (n, player, status) = chunk(">BBB", decoded)
                        for ke, va in enums.StatusFeeling.items():
                            if status == va:
                                status = "%s:%s" % (status, ke)
                                break
                        for x in 'player status'. split():
                            print x, locals()[x]
                    elif k == 'UseAttack':
                        decoded, (n, player, move) = chunk(">BBH", decoded)
                        for ke, va in enums.Move.items():
                            if move == va:
                                move = "%s:%s" % (move, ke)
                                break
                        for x in 'player move'. split():
                            print x, locals()[x]
                    elif k == 'MoveMessage':
                        decoded, (n, player, move, part, type_) = chunk(">BBHBB", decoded)
                        if decoded:
                            decoded, (foe,) = chunk(">B", decoded)
                        else:
                            foe = 0
                        if decoded:
                            decoded, (other,) = chunk(">H", decoded)
                        else:
                            other = 0
                        type_ = find_in(type_, enums.Type)
                        other_ = find_in(other, enums.Move, enums.Ability)
                        try:
                            message = enums.move_message[move][part]
                        except IndexError:
                            message = 'Unknown message %s[%s]' % (move, part)
                        try:
                            msg_parts = []
                            for i, p in enumerate(message.split('%')):
                                if i == 0:
                                    msg_parts.append(p[:1])
                                elif p[0] == 's':
                                    msg_parts.append('{src %s}' % player)
                                elif p[0] == 'f':
                                    msg_parts.append('{foe %s}' % foe)
                                elif p[0] == 't':
                                    msg_parts.append('%s' % type_)
                                elif p[0] == 'm':
                                    msg_parts.append('%s' % find_in(other, enums.Move))
                                else:
                                    msg_parts.append("[??? %s ???]" % p[0])
                                msg_parts.append(p[1:])
                            print ''.join(msg_parts)
                        except Exception, e:
                            print type(e), e
                            print message
                        for x in 'player move part type_ foe other_ decoded'. split():
                            print x, locals()[x]
                    elif k == 'AbilityMessage':
                        decoded, (n, player, move, part) = chunk(">BBHB", decoded)
                        if decoded:
                            decoded, (type_,) = chunk(">B", decoded)
                        else:
                            type_ = 0
                        if decoded:
                            decoded, (foe,) = chunk(">B", decoded)
                        else:
                            foe = 0
                        if decoded:
                            decoded, (other,) = chunk(">H", decoded)
                        else:
                            other = 0
                        type_ = find_in(type_, enums.Type)
                        other_ = find_in(other, enums.Move, enums.Ability)
                        try:
                            message = enums.ability_messages[move][part]
                        except IndexError:
                            message = 'Unknown message %s[%s]' % (move, part)
                        try:
                            msg_parts = []
                            for i, p in enumerate(message.split('%')):
                                if i == 0:
                                    msg_parts.append(p[:1])
                                elif p[0] == 's':
                                    msg_parts.append('{src %s}' % player)
                                elif p[0] == 'f':
                                    msg_parts.append('{foe %s}' % foe)
                                elif p[0] == 'a':
                                    msg_parts.append('%s' % find_in(other, enums.Ability))
                                else:
                                    msg_parts.append("[??? %s ???]" % p[0])
                                msg_parts.append(p[1:])
                            print ''.join(msg_parts)
                        except Exception, e:
                            print type(e), e
                            print message
                        for x in 'player move part type_ foe other_ decoded'. split():
                            print x, locals()[x]
                    elif k == 'ItemMessage':
                        decoded, (n, player, move, part) = chunk(">BBHB", decoded)
                        if decoded:
                            decoded, (foe,) = chunk(">B", decoded)
                        else:
                            foe = 0
                        if decoded:
                            decoded, (berry,) = chunk(">H", decoded)
                        else:
                            berry = 0
                        if decoded:
                            berry, (stat,) = chunk(">H", decoded)
                        else:
                            stat = 0
                        try:
                            message = enums.item_messages[move][part]
                        except IndexError:
                            message = 'Unknown message %s[%s]' % (move, part)
                        try:
                            msg_parts = []
                            for i, p in enumerate(message.split('%')):
                                if i == 0:
                                    msg_parts.append(p[:1])
                                elif p[0] == 's':
                                    msg_parts.append('{src %s}' % player)
                                elif p[0] == 'f':
                                    msg_parts.append('{foe %s}' % foe)
                                else:
                                    msg_parts.append("[??? %s ???]" % p[0])
                                msg_parts.append(p[1:])
                            print ''.join(msg_parts)
                        except Exception, e:
                            print type(e), e
                            print message
                        for x in 'player move part foe berry stat'. split():
                            print x, locals()[x]
                    elif k == 'StatChange':
                        decoded, (n, player, stat, delta) = chunk(">BBBB", decoded)
                        stat = find_in(stat, enums.Stat)
                        for x in 'player stat delta'. split():
                            print x, locals()[x]
                    else:
                        continue
                    if decoded:
                        print 'rest', repr(decoded)
                    break
