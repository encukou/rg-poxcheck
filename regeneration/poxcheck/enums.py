"""A quick-and-dirty parser of PO's enums

The enums will become module-level dictionaries here.
"""

import sys
import os

from regeneration.poxcheck import po_root

__copyright__ = 'Copyright 2010-2011, Petr Viktorin'
__license__ = 'MIT'
__email__ = 'encukou@gmail.com'

class DumbCaseInsensitiveDict(dict):
    def __init__(self):
        self.__keys = set()

    def __setitem__(self, key, value):
        self.__keys.add(key)
        key = key.lower()
        super(DumbCaseInsensitiveDict, self).__setitem__(key, value)
        key = key.replace('_', '')
        super(DumbCaseInsensitiveDict, self).__setitem__(key, value)
        key = key.replace(' ', '')
        super(DumbCaseInsensitiveDict, self).__setitem__(key, value)

    def __getitem__(self, key):
        try:
            return super(DumbCaseInsensitiveDict, self).__getitem__(key.lower())
        except KeyError:
            return super(DumbCaseInsensitiveDict, self).__getitem__(
                    key.replace(' ', '').lower())

    def __iter__(self):
        return iter(self.__keys)

    def items(self):
        for key in self.__keys:
            yield key, self[key]

def update_globals(*files):
    currentEnum = None
    nsname = "Anonymous"
    for line in open(os.path.join(po_root, *files)):
        line = line.strip()
        structtype, sep, name = line.partition(' ')
        if name.endswith('{'):
            name = name[:-1].strip()
        if structtype == 'namespace':
            nsname = name
        elif structtype == 'enum':
            if name == 'Name' or not name:
                name = nsname
            currentEnum = globals()[name] = DumbCaseInsensitiveDict()
            currentEnumName = name
            nextValue = 0
        elif '}' in line:
            currentEnum = None
        elif '{' in line:
            pass
        elif currentEnum is not None:
            line, dummy, oneline_comment = line.partition('//')
            line, dummy, dummy = line.partition('/*')
            line = line.replace('*/', '')
            line = line.strip()
            if line.endswith(','):
                line = line[:-1]
            name, eq, value = line.partition('=')
            name = name.strip()
            value = value.strip()
            if eq:
                try:
                    value = int(value)
                except Exception:
                    value = eval(value, currentEnum)
            else:
                value = nextValue
            if name not in 'ModeFirst ModeLast'.split():
                for k, v in currentEnum.items():
                    assert v != value, (
                        currentEnumName, k, name, v, value, files)
            currentEnum[name] = value
            if oneline_comment and '@@=' in oneline_comment:
                dummy, dummy, c_name = oneline_comment.partition('@@=')
                currentEnum[c_name.replace(' ', '')] = value
            if isinstance(value, int):
                nextValue = value + 1
            else:
                nextValue = None
                del nextValue

def update_messages(*files):
    g = globals()[os.path.splitext(files[-1])[0]] = []
    for line in open(os.path.join(po_root, *files)):
        g.append(line.strip().split('|'))

update_globals('src', 'Server', 'battle.h')
update_globals('src', 'P''okemonInfo', 'enums.h')
update_globals('src', 'P''okemonInfo', 'battlestructs.h')
update_globals('src', 'Server', 'analyze.h')
update_globals('src', 'Teambuilder', 'basebattlewindow.h')
update_globals('src', 'Server', 'battleinterface.h')
update_globals('src', 'Shared', 'networkcommands.h')
update_globals('src', 'Shared', 'battlecommands.h')

update_messages('bin', 'db', 'moves', 'move_message.txt')
update_messages('bin', 'db', 'abilities', 'ability_messages.txt')
update_messages('bin', 'db', 'items', 'item_messages.txt')

# Fixups
Item[''] = Item['NoItem']
