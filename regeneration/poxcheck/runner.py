# Encoding: UTF-8

import os
import sys
from threading import Lock
import random
import time
import itertools
import datetime
import multiprocessing
from collections import defaultdict

#import pygit2

import sqlalchemy
from sqlalchemy import func
import sqlalchemy.ext.declarative

import execnet
from fabulous import color

import regeneration.battle
import regeneration.pokey
import regeneration.poxcheck


stdout_lock = Lock()
getspec_lock = Lock()

metadata = sqlalchemy.MetaData()
TableBase = sqlalchemy.ext.declarative.declarative_base(metadata=metadata)

testVersion = 0

class TestResult(TableBase):
    __tablename__ = 'test_results'
    impl = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True, nullable=False)
    seed = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True, nullable=False)
    first_version = sqlalchemy.Column(sqlalchemy.Text, index=True, nullable=False)
    last_version = sqlalchemy.Column(sqlalchemy.Text, index=True, nullable=False)
    last_checked = sqlalchemy.Column(sqlalchemy.DateTime, nullable=False, default=False)
    result = sqlalchemy.Column(sqlalchemy.VARCHAR(2), index=True, nullable=False, default=False)
    output = sqlalchemy.Column(sqlalchemy.Unicode, nullable=True)
    num_turns = sqlalchemy.Column(sqlalchemy.Integer, nullable=False, default=False)
    time = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=False)
    best_time = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=False)
    bug = sqlalchemy.Column(sqlalchemy.Text, nullable=True)

    def one_char(self, ver):
        fp = int(self.first_version.split('.')[0].split('-')[1])
        lp = int(self.last_version.split('.')[0].split('-')[1])
        if True or self.time < self.best_time * 2:
            colorfunc = color.fg256
        else:
            colorfunc = color.bg256
        if ver == self.last_version:
            full = 250
            mid = 100
            none = 0
            getchar = lambda s: s[0]
        else:
            full = int(180 * (0.99 ** (lp - fp)))
            mid = int(50 * (0.99 ** (lp - fp)))
            none = 0
            getchar = lambda s: s[1]
        if self.result == 'OK':
            result = colorfunc((none, full, none), getchar(u'•·'))
        elif self.result == 'E':
            result = colorfunc((full, mid, none), getchar('Ee'))
        elif self.result == 'F':
            result = colorfunc((full, none, none), getchar('Ff'))
        else:
            result = colorfunc((full, full, full), self.result[0])
        return result.as_utf8

def get_session(dburl=None):
    if not dburl:
        dburl = 'sqlite:///' + os.path.join(os.path.dirname(__file__),
            'testresults.sqlite')

    engine = sqlalchemy.create_engine(dburl)
    connection = engine.connect()
    metadata.bind = engine
    metadata.create_all()
    sm = sqlalchemy.orm.sessionmaker(
            autoflush=True,
            autocommit=False,
            bind=engine
        )
    session = sqlalchemy.orm.scoped_session(sm)
    return session

def roundrobin(*iterables):
    "roundrobin('ABC', 'D', 'EF') --> A D E B F C"
    # Recipe credited to George Sakkis
    pending = len(iterables)
    nexts = itertools.cycle(iter(it).next for it in iterables)
    while pending:
        try:
            for next in nexts:
                yield next()
        except StopIteration:
            pending -= 1
            nexts = itertools.cycle(itertools.islice(nexts, pending))

class getspecs(object):
    def __init__(self, version, session):
        self.version = version
        self.session = session
        self.max_seeds = dict(session.query(
                        TestResult.impl,
                        sqlalchemy.func.max(TestResult.seed),
                    )
                .filter(TestResult.result != 'NI')
                .group_by(TestResult.impl)
            )
        if self.max_seeds:
            self.max_impl = max(self.max_seeds)
        else:
            self.max_impl = -4

    def __iter__(self):
        done = set()
        iterable = roundrobin(
                self.fill_min(),
                self.redo_results(['E', 'F']),
                self.redo_results(['E']),
                self.redo_old(),
                self.check_next(),
                self.fill_min(),
                itertools.repeat(None))
        for val in itertools.ifilterfalse(done.__contains__, iterable):
            if not val:
                yield val
            elif not val in done and val[1] < 100:
                yield val
                done.add(val)

    def check_next(self):
        while True:
            yield None
            try:
                result, version = (self.session.query(
                                TestResult.result, TestResult.last_version)
                        .filter(TestResult.impl == self.max_impl + 1)
                        .filter(TestResult.seed == 0)
                    ).one()
            except sqlalchemy.orm.exc.NoResultFound:
                yield self.max_impl + 1, 0
            else:
                if result == 'NI':
                    if version == self.version:
                        return
                    else:
                        yield self.max_impl + 1, 0
                else:
                    self.max_impl += 1

    def fill_min(self):
        while True:
            yield None
            if self.max_seeds:
                for impl, s in sorted(self.max_seeds.items(),
                            key=lambda (k, v): (v >= 100, v + k * 2, random.random())):
                    if not (self.session.query(TestResult.seed)
                            .filter(TestResult.impl == impl)
                            .filter(TestResult.result == 'OK')
                        ).count():
                        continue
                    seed, = (self.session.query(
                                    sqlalchemy.func.max(TestResult.seed) + 1,
                                )
                            .filter(TestResult.impl == impl)
                        ).one()
                    if seed is not None:
                        yield impl, seed
                        self.max_seeds[impl] = seed
                    break

    def redo_results(self, results):
        while True:
            yield None
            query = (self.session.query(
                                TestResult.impl,
                                TestResult.seed,
                            )
                        .filter(TestResult.result.in_(results))
                        .order_by(TestResult.last_version == self.version)
                        .order_by(TestResult.first_version.asc())
                        .order_by(TestResult.best_time / TestResult.time)
                    )
            to_redo = query[:100]
            random.shuffle(to_redo)
            for impl, seed in to_redo[:10]:
                yield impl, seed
            for i in range(5):
                yield None

    def redo_old(self):
        while True:
            yield None
            query = (self.session.query(
                                TestResult.impl,
                                TestResult.seed,
                            )
                        .filter(TestResult.last_version != self.version)
                        .order_by(TestResult.last_version.asc())
                        .order_by(TestResult.best_time / TestResult.time)
                    )
            old = query[:100]
            random.shuffle(old)
            for impl, seed in old[:10]:
                yield impl, seed

def process_result(session, version, spec, result_tuple):
    try:
        impl, seed = spec
        result, output, elapsed_time, num_turns = result_tuple
        record = session.query(TestResult).get((impl, seed))
        if not record:
            comment = 'fresh'
            record = TestResult(
                    impl=impl,
                    seed=seed,
                    first_version=version,
                    result=result,
                    best_time=elapsed_time,
                )
            session.add(record)
        else:
            if record.result != result:
                comment = 'changed from %s' % record.result
                record.result = result
                record.first_version = version
                record.best_time = elapsed_time
            elif record.best_time > elapsed_time:
                comment = 'better'
                record.best_time = elapsed_time
            else:
                comment = ''
        record.last_version = version
        record.output = output
        record.num_turns = num_turns
        record.time = elapsed_time
        record.last_checked = datetime.datetime.today()
        session.commit()
        with stdout_lock:
            print 'result for {0[0]:3} {0[1]:3}: {1:3.3} {2:5.2} {3}'.format(
                    spec, result, elapsed_time, comment)
    except:
        import traceback
        traceback.print_exc()
        raise

def remote_func(channel):
    import sys
    import time
    import traceback
    sys.stdout = open('/dev/null', 'w')
    sys.stderr = open('/dev/null', 'w')

    from regeneration.poxcheck.runner import get_version
    version = get_version()
    channel.send(version)
    s = 0

    import itertools
    from regeneration.poxcheck.checker import Checker
    from regeneration.poxcheck.testbattle import battle_and_rules
    error_counter = 0
    for i in itertools.count():
        spec = channel.receive()
        turn_number = 0
        elapsed_time = 0.0
        try:
            battle_dict, rules = battle_and_rules(*spec)
        except ValueError, e:
            result = 'NI'
            output = traceback.format_exc()
        else:
            try:
                start_time = time.clock()
                checker = None
                checker = Checker(battle_dict, rules, quiet=True)
                winner = checker.run()
                result = 'OK'
                output = str(winner)
                turn_number = checker.turn_number
            except AssertionError, e:
                result = 'F'
                output = traceback.format_exc()
                if checker:
                    turn_number = checker.turn_number
                else:
                    turn_number = -1
            except Exception, e:
                result = 'E'
                output = traceback.format_exc()
            finally:
                elapsed_time = time.clock() - start_time
        channel.send((i, spec, (result, output, elapsed_time,turn_number)))
        if result == 'E':
            error_counter += 1
            if error_counter > 2:
                # Too buggy
                break
        else:
            error_counter = 0
        '''
        import time
        time.sleep(0.1)
        from guppy import hpy
        h=hpy()
        print>>sys.__stderr__, h.heap()
        import gc
        o=0
        obj = max((o for o in gc.get_objects()), key=sys.getsizeof)
        print>>sys.__stderr__,  type(obj), obj
        '''

def setup_new_gateway(group, session, currently_processed):
    gateway = group.makegateway()
    gateway._current_job = None

    channel = gateway.remote_exec(remote_func)

    try:
        version = channel.receive()
    except execnet.gateway_base.RemoteError, e:
        print e
        return

    specs = iter(getspecs(version, session))

    def _process_and_next(i_spec_result):
        i, spec, result = i_spec_result
        process_result(session, version, spec, result)
        with getspec_lock:
            currently_processed.remove(spec)
        if (i + 1) % 5 == 0:
            # Check if there's a new version
            new_version = get_version()
            if new_version != version:
                # New version. Exit.
                gateway.exit()
                with stdout_lock:
                    print "New version available."
                return
        _next()
    def _next():
        for i in range(100):
            spec = specs.next()
            with getspec_lock:
                if spec and spec not in (gw._current_job for gw in group):
                    #with stdout_lock:
                    #    print 'sending', spec
                    channel.send(spec)
                    currently_processed.add(spec)
                    gateway._current_job = spec
                    break
        else:
            with stdout_lock:
                print 'Nothing to do!'
            gateway.exit()
    channel.setcallback(_process_and_next)
    _next()
    some_channel = channel
    return channel

def print_report(session):
    results = list(session.query(TestResult)
            .order_by(TestResult.impl, TestResult.seed)
        )
    avg_time, avg_turn = session.query(func.avg(TestResult.time),
            func.sum(TestResult.time) / func.sum(TestResult.num_turns)).one()
    ver = get_version()
    this_avg_time, this_avg_turn = session.query(func.avg(TestResult.time),
            func.sum(TestResult.time) / func.sum(TestResult.num_turns)).filter(
            TestResult.last_version == ver).one()
    bad_results = session.query(TestResult).filter(
            TestResult.result.in_(('E', 'F'))).all()
    max_non_ni, = session.query(func.max(TestResult.impl)).filter(
            TestResult.result != 'NI').one()
    num_ok = session.query(TestResult).filter_by(result='OK', last_version=ver).count()
    num_ni = session.query(TestResult).filter_by(result='NI', last_version=ver).count()
    num_thisver = session.query(TestResult).filter_by(last_version=ver).count()
    with stdout_lock:
        impl_before = None
        print "      ",
        for i in range(10, 100, 10):
            print "{0:9}".format(i // 10),
        print "\n     ",
        for i in range(0, 100):
            sys.stdout.write(format(i % 10))
        for result in results:
            if impl_before != result.impl:
                print '\n{0:4} '.format(result.impl),
                impl_before = result.impl
            sys.stdout.write(result.one_char(ver))
        print
        if avg_turn:
            print 'Average time: {0:.2f}s ({1:.2f}ms/turn)'.format(avg_time, 1000 * avg_turn)
        if this_avg_turn:
            print 'This version: {0:.2f}s ({1:.2f}ms/turn)'.format(this_avg_time, 1000 * this_avg_turn)
        bresult_groups = defaultdict(set)
        for r in bad_results:
            if r.bug:
                bresult_groups[r.result + '! ' + r.bug].add('[%s %s]' % (r.impl, r.seed))
            else:
                bresult_groups[r.result + '?' + str(r.impl).rjust(3)].add(r.seed)
        for c, rs in sorted(bresult_groups.items(),
                key=lambda (x, y): (isinstance(x,int), x)):
            print '%s:' % c,
            for r in sorted(rs):
                print r,
            print
        print '%s bad results' % len(bad_results)
        if max_non_ni:
            total = max_non_ni * 100
            min_good = 100. * num_ok / total
            max_good = 100. * (total - len(bad_results)) / total
            print "Total success rate: {0:.1f}-{1:.1f}%".format(min_good, max_good),
            if num_thisver:
                abo_good = 100. * num_ok / (num_thisver - num_ni)
                print "(this version: {0:.2f}%)".format(abo_good)
            else:
                print

def main(max_subprocesses=None, len_impl=3):
    if max_subprocesses is None:
        max_subprocesses = multiprocessing.cpu_count()
        if max_subprocesses == 1:
            max_subprocesses = 2
    group = execnet.Group()
    session = get_session()
    currently_processed = set()
    last_job = None
    try:
        for i in itertools.count():
            if group:
                if last_job == group[0]._current_job or i % 12 == 0:
                    group[0].exit()
                else:
                    last_job = group[0]._current_job
            if len(group) < max_subprocesses:
                setup_new_gateway(group, session, currently_processed)
            print len(group), 'slaves:', ' '.join(
                    str(gw._current_job) for gw in group)
            print_report(session)
            time.sleep(60 / max_subprocesses * len(group))
    finally:
        print_report(session)

def get_version():
    po_root = regeneration.poxcheck.po_root
    return '.'.join((
            'B-' + dir_version(regeneration.battle.__file__),
            'P-' + dir_version(regeneration.pokey.__file__),
            'X-' + dir_version(__file__),
            'PO-' + dir_version(os.path.join(po_root, 'bin')),
        ))

def dir_version(filename, compute_hash=True):
    template = '{crude_revno:04}-{sha:10.10s}-{hash:08x}'
    dirname = os.path.dirname(filename)
    if compute_hash:
        mtimes = set()
        for dirpath, dirnames, filenames in os.walk(dirname):
            for filename in filenames:
                if filename.endswith(('.py', 'Server')):
                    filename = os.path.abspath(os.path.join(dirpath, filename))
                    mtimes.add((filename, os.stat(filename).st_mtime))
        mtime_hash = hash(frozenset(mtimes)) & 0xffffffff
    else:
        mtime_hash = 0
    dirname = os.path.join(dirname, 'dir')
    while False and os.path.abspath(os.path.join(dirname, '..')) != dirname:
        dirname = os.path.abspath(os.path.join(dirname, '..'))
        try:
            repo = pygit2.Repository(os.path.join(dirname, '.git'))
        except pygit2.GitError:
            continue
        else:
            break
    else:
        return template.format(crude_revno=0, sha='0' * 64, hash=mtime_hash)
    head = repo.lookup_reference('HEAD').resolve()
    crude_revno = sum(1 for c in repo.walk(head.sha, pygit2.GIT_SORT_TIME))
    return template.format(
            crude_revno=crude_revno,
            sha=head.sha,
            hash=mtime_hash,
        )

def setbug():
    impl = int(sys.argv[1])
    seed = int(sys.argv[2])
    bug = ' '.join(sys.argv[3:])
    session = get_session()
    r = session.query(TestResult).filter_by(impl=impl, seed=seed).one()
    r.bug = bug
    session.commit()

if __name__ == '__main__':
    try:
        main()
    finally:
        '''
        from guppy import hpy
        h=hpy()
        print h.heap()
        print h.heap().sp

        import gc
        obj = max((o for o in gc.get_objects() if type(o) in (str, tuple)), key=sys.getsizeof)
        print type(obj), obj
        '''

