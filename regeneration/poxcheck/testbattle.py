# Encoding: UTF-8

import random
import sys
from ast import literal_eval

import yaml
import re
import os

from sqlalchemy.orm import aliased

from pokedex.db import connect, tables
from regeneration.battle.gender import Gender
from regeneration.battle.rules import Rules as BaseRules
from regeneration.battle.example import loader as base_loader
from regeneration.battle.trainer import Trainer

from regeneration.pokey.rules import PokeyRules
from regeneration.pokey.loader import default_loader
from regeneration.pokey.monster import Monster

from regeneration.poxcheck.lrucache import lru_cache

__copyright__ = 'Copyright 2011, Petr Viktorin'
__license__ = 'MIT'
__email__ = 'encukou@gmail.com'

__version__ = '3'

session = connect()

def get_things(table, identifiers, query=None):
    if query is None:
        query = session.query(table.identifier, table)
    query = query.filter(table.identifier.in_(identifiers))
    things = dict(query)
    return [things[i] for i in identifiers]

implemented_abilities = get_things(tables.Ability, 'trace download swarm '
        'technician pressure limber shed-skin own-tempo synchronize sturdy '
        'levitate shield-dust '
        # D1
        'overgrow blaze torrent water-veil illuminate frisk rivalry thick-fat '
        'natural-cure shell-armor battle-armor guts serene-grace color-change '
        'soundproof iron-fist '#wonder-guard static '#flame-body rough-skin sniper '
#        #'hustle water-absorb flash-fire volt-absorb normalize pure-power '
        .split())
implemented_moveeffects = get_things(tables.Move,
        query=session.query(tables.Move.identifier, tables.MoveEffect)
            .filter(tables.Move.effect_id == tables.MoveEffect.id),
        identifiers='tackle tri-attack trick-room sharpen conversion agility '
            'conversion-2 psybeam recover icy-wind magnet-rise lock-on '
            # D1
            'discharge ice-beam flamethrower harden double-team '
            'quick-attack growl tail-whip string-shot flash sweet-scent '
            'confuse-ray charm screech scary-face metal-sound iron-tail '
            'mist-ball swift psychic mud-slap metal-claw ancientpower overheat '
            'charge-beam aurora-beam tickle bulk-up calm-mind dragon-dance '
            'hammer-arm close-combat shift-gear shell-smash tail-glow coil '
            'work-up cotton-guard glaciate curse splash will-o-wisp'.split(),
    )
implemnted_item_identifiers = ('nugget pearl big-pearl brightpowder leftovers '
        'razor-claw lax-incense flame-orb wise-glasses choice-specs '
        'air-balloon shell-bell '
        # D1
        'choice-band choice-scarf black-belt blackglasses charcoal dragon-fang '
        'hard-stone magnet metal-coat miracle-seed mystic-water nevermeltice '
        'odd-incense poison-barb rock-incense rose-incense sea-incense '
        'sharp-beak silk-scarf silverpowder soft-sand spell-tag twistedspoon '
        'wave-incense'
            .split())#[:-1]

natures = tuple(session.query(tables.Nature))

@lru_cache(1000)
def legal_moves(pokemon_id, effect_ids):
    query = session.query(tables.Move)
    query = query.join(tables.Move.pokemon_moves)
    query = query.filter(tables.PokemonMove.pokemon_id == pokemon_id)
    query = query.filter(tables.Move.effect_id.in_(effect_ids))
    query = query.distinct()
    return frozenset(query)

@lru_cache(1000)
def _for_impl(impl):
    impl_l = impl // 3 + 1
    if any((
            len(implemented_abilities) < impl_l,
            len(implemented_moveeffects) < impl_l,
            len(implemnted_item_identifiers) < impl_l,
        )):
        raise ValueError('Not implemented')
    if impl < 0:
        impl_l = len(implemented_abilities)
    allowed_abilities = [a.id for a in implemented_abilities[:impl_l]]
    allowed_moveeffects = [m.id for m in implemented_moveeffects[:impl_l]]
    allowed_items = implemnted_item_identifiers[:impl_l]
    target_moveeffects = allowed_moveeffects
    if impl >= 0:
        if impl % 3 == 0:
            target_moveeffects = allowed_moveeffects[-1:]
        elif impl % 3 == 1:
            allowed_abilities = allowed_abilities[-1:]
        elif impl % 3 == 2:
            allowed_items = allowed_items[-1:]

    query = session.query(tables.PokemonForm, tables.Ability,
            tables.Move)
    query = query.join(tables.PokemonForm.pokemon)
    query = query.join(tables.Pokemon.all_abilities)
    query = query.filter(tables.PokemonAbility.ability_id == tables.Ability.id)
    query = query.join(tables.Pokemon.pokemon_moves)
    query = query.filter(tables.PokemonMove.move_id == tables.Move.id)
    query = query.filter(tables.Move.effect_id.in_(target_moveeffects))
    query = query.filter(tables.Ability.id.in_(allowed_abilities))
    query = query.order_by(tables.PokemonForm.id, tables.Ability.id,
            tables.Move.id)
    query = query.distinct()
    return tuple(query), tuple(allowed_moveeffects), tuple(allowed_items)

def generate_battle(impl, seed, battle_format=[[0], [1]], trainer_names=None):
    if battle_format == [[0], [1]]:
        format_name = '1v1'
    else:
        format_name = str(battle_format)
    try:
        from regeneration import regressiontest
    except ImportError:
        filename = None
        raise
    else:
        filename = os.path.join(
                os.path.dirname(regressiontest.__file__),
                'data', 'battles',
                format_name,
                '{0:03}'.format(impl),
                '{0:03}.yaml'.format(seed))
        if os.path.exists(filename):
            try:
                battle = yaml.safe_load(open(filename))
            except:
                pass
            else:
                if (isinstance(battle, dict) and
                        battle.get('testbattle_version') == __version__ and
                        battle.get('testbattle_args') ==
                                [format_name, impl, seed]):
                    return battle
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
    f_a_m, allowed_moveeffects, allowed_items = _for_impl(impl)
    rand = random.Random(hash((impl, seed)))
    taken_names = set()
    def get_pk():
        form, ability, move1 = rand.choice(f_a_m)
        moves = set(legal_moves(form.pokemon.id, allowed_moveeffects))
        moves.discard(move1)
        if len(moves) <= 3:
            moves = [move1] + list(moves)
        else:
            moves = [move1] + rand.sample(list(moves), 3)
        gender = Gender.random(form.species.gender_rate, rand)
        stat_idents = (
            'hp attack defense speed special-attack special-defense'.split())
        bare_name = form.name.replace(' ', '').replace(u'♂', '-M').replace(u'♀', '-F')
        name = bare_name[:10]
        i = 1
        while name in taken_names:
            postfix = '-%s' % i
            name = bare_name[:10 - len(postfix)] + postfix
            name = re.sub(r'([^a-zA-Z0-9])[^a-zA-Z0-9]+', r'\1', name)
            i += 1
        taken_names.add(name)
        level = rand.randint(50, 100)
        shiny = rand.randint(0, 8191) == 0
        item = rand.choice(allowed_items)
        if item == 'no-item':
            item = None
        nature = rand.choice(natures).identifier
        genes = dict((n, rand.randint(0, 31)) for n in stat_idents)
        effort = dict((n, rand.randint(0, 85)) for n in stat_idents)
        tameness = rand.randint(0, 255)
        ability = ability.identifier
        if form.species.identifier == 'giratina':
            if item == 'griseous-orb':
                form_identifier = 'origin'
                ability = 'levitate'
            else:
                form_identifier = 'altered'
                ability = 'pressure'
        else:
            if item == 'griseous-orb':
                item = None
            form_identifier = form.form_identifier
        if form_identifier == 'pirouette':
            form_identifier = 'aria'
        dct = dict(
                nickname=name,
                species=form.species.identifier,
                form=form_identifier,
                level=level,
                shiny=shiny,
                item=item,
                gender=gender.identifier,
                nature=nature,
                ability=ability,
                genes=genes,
                effort=effort,
                tameness=tameness,
                met='',
                status='ok',
                moves=[dict(kind=m.identifier, pp=m.pp * 8 // 5) for m in moves],
            )
        monster = Monster.load(dct, default_loader)
        dct['stats'] = dict((k.identifier, v) for k, v in monster.stats.items())
        dct['hp'] = monster.hp
        return dct
    trainers = dict()
    for side in battle_format:
        for i, identifier in enumerate(side):
            if identifier not in trainers:
                if isinstance(identifier, int):
                    names = (trainer_names or
                        'Red Blue Yellow Gold Silver Crystal'.split())
                    try:
                        name = names[identifier]
                    except IndexError:
                        name = 'trainer_%s' % identifier
                else:
                    name = identifier
                trainers[identifier] = dict(
                        name=name,
                        team=[get_pk() for i in range(6)],
                        seed=rand.randint(0, 2**62),
                    )
    battle = dict(
            seed=rand.randint(0, 2**62),
            battle_format=battle_format,
            trainers=trainers,
            testbattle_version = __version__,
            testbattle_args = (format_name, impl, seed),
        )
    if filename:
        yaml.safe_dump(battle, open(filename, 'w'))
    return battle

base_rules = BaseRules(base_loader)
pokey_rules = PokeyRules(default_loader)

def battle_and_rules(impl, seed, trainer_names=None):
    if impl < 0:
        rules = base_rules
        impl = -impl - 1
    else:
        rules = pokey_rules
    battle = generate_battle(impl, seed, rules.format,
            trainer_names=trainer_names)
    return battle, rules

if __name__ == '__main__':
    args = sys.argv
    run = ('-r' in args)
    if run:
        args.remove('-r')
    args = dict(enumerate(args))
    impl = int(args.get(1, -1))
    seed = int(args.get(2, random.randint(0, 2**128)))
    b, r = battle_and_rules(impl, seed)
    print(yaml.safe_dump(b))
    if run:
        from regeneration.battle.field import Field
        from regeneration.battle import messages
        field = Field.load(b, r.loader,
                trainer_loader=lambda *args, **kwargs:
                        Trainer.load(*args,
                        monster_class=r.MonsterClass, **kwargs))
        def printer(message):
            if isinstance(message, messages.TurnStart):
                print
            if message.shown is not None:
                print message
        field.add_observer(printer)
        field.run()
