#! /usr/bin/env python
# Encoding: UTF-8

import os
import sys
import yaml
import time
import gzip
import struct
import random
import base64
import tempfile
import resource
import itertools
import subprocess
import simplejson
from fractions import Fraction

try:
    from yaml import CSafeLoader as SafeLoader, CSafeDumper as SafeDumper
except ImportError:
    from yaml import SafeLoader, SafeDumper

from fabulous import color

from regeneration.battle.stats import Stats
from regeneration.battle.field import Field
from regeneration.battle.trainer import Trainer
from regeneration.pokey import messages
from regeneration.pokey import moveeffects
from regeneration.battle import example

from regeneration.poxcheck import po_root
from regeneration.poxcheck import enums
from regeneration.poxcheck.analyze import analyze
from regeneration.poxcheck import testbattle


__copyright__ = 'Copyright 2009-2011, Petr Viktorin'
__license__ = 'MIT'
__email__ = 'encukou@gmail.com'

__version__ = '8'

def translateName(name):
    print 'Translating', name
    try:
        name, forme = name
    except ValueError:
        name = name.replace(' ', '').replace('-', '').replace("'", '')
        return {
                # Species
                "Porygon-Z": "Porygonz",
                "Ho-Oh": "Ho-oh",
                u"Nidoran♀": "Nidoran_F",
                u"Nidoran♂": "Nidoran_M",
                # Forms
                "normal:forme": '',
                "altered:forme": '',
                "land:forme": '',
                # (obsolete forms)
                "mow:forme": "cut",
                "fan:forme": "s?",
                "plant:forme": '',
                "sandy:forme": '',
                "trash:forme": '',
                "burn:forme": '',
                "shock:forme": '',
                "chill:forme": '',
                "douse:forme": '',
                "spring:forme": '',
                "summer:forme": '',
                "autumn:forme": '',
                "winter:forme": '',
                "aria:forme": '',
                "pirouette:forme": '',
                "spikyeared:forme": '',
                # Items
                "BrightPowder": "Brightpowder",
            }.get(name, name)
    else:
        formeSuffix = translateName(forme + ':forme')[:1]
        if formeSuffix:
            print 'Form suffix:', formeSuffix
            return translateName(name) + '_' + formeSuffix
        else:
            print 'No form suffix'
            return translateName(name)

class ColorStream(object):
    def __init__(self, stream, color):
        self.stream = stream
        self.color = color
        self.buf = ''

    def write(self, string):
        # Poor man's attempd at solving multiplexing issues with the stdout...
        self.buf += string
        while '\n' in self.buf:
            bef, sep, self.buf = self.buf.rpartition('\n')
            self.stream.write(color.fg256(self.color, bef).as_utf8)
            self.stream.flush()
            self.stream.write('\n')
            self.stream.flush()
            time.sleep(0)

    def flush(self):
        self.stream.write(color.fg256(self.color, self.buf).as_utf8)
        self.buf = ''
        self.stream.flush()
        time.sleep(0)

    @property
    def name(self):
        return self.stream.name

class ColoredStream(object):
    def __init__(self, obj, attr, color):
        self.obj = obj
        self.attr = attr
        self.color = color
        self.stack = []

    def __enter__(self):
        orig = getattr(self.obj, self.attr)
        self.stack.append(orig)
        if isinstance(orig, ColorStream):
            orig = orig.stream
        if getattr(orig, 'name', '') != '/dev/null':
            setattr(self.obj, self.attr, ColorStream(orig, self.color))

    def __exit__(self, *junk):
        setattr(self.obj, self.attr, self.stack.pop())

gender_table = dict(male=1, female=2, none=0)

class TestTrainer(Trainer):
    _prev_resp = {}

    def __init__(self, *args, **kwargs):
        self.checker = kwargs.pop('checker')
        super(TestTrainer, self).__init__(*args, **kwargs)

    def replace_request_command(self, func):
        self.request_command = lambda request: func(
            super(TestTrainer, self).request_command(request))

    def request_command(self, request):
        with ColoredStream(sys, 'stdout', (150, 150, 200)):
            command = super(TestTrainer, self).request_command(request)

            switches_allowed = bool(list(request.switches()))
            switches_allowed = True
            mo_set = list(c.move for c in request.moves())
            print '$$$', [(m.name, m.pp) for m in mo_set]
            moves_allowed = bool(mo_set)
            mo_allowed = []
            for m in request.battler.moves:
                mo_allowed.append(m in mo_set and not request.battler.fainted)
            while len(mo_allowed) < 4:
                mo_allowed.append(False)
            tslot = [s.spots[0].trainer for s in request.field.sides].index(self)
            prefix = 'B:0:%s:$' % tslot
            data = struct.pack(">BBB??????", 3, tslot, tslot, switches_allowed, moves_allowed, *mo_allowed)
            print 'COMMAND:', command
            if command.command == 'move':
                move, target = command.move, command.target
                try:
                    moveindex = request.battler.moves.index(move)
                except ValueError:
                    moveindex = -1
                targetTrainerNumber = (1 - tslot)
                if target is None:
                    targetPokemon = 0
                else:
                    for i, opponent_spot in enumerate(s for s in request.field.spots if s.trainer != self):
                        if target.spot == opponent_spot:
                            targetPokemon = i
                    targetPokemon += targetTrainerNumber * 1  # 1 is the (default) team size # XXX: Adjust for double battle
                #self.send_command()
                cmd_data_bin = ata = struct.pack(">BBbB", tslot, enums.ChoiceType['AttackType'], moveindex, targetPokemon)
                print "Attack type:", enums.ChoiceType['AttackType'], "in", repr(cmd_data_bin)
                do_wait = True
            elif command.command == 'switch':
                pokemon = command.replacement
                pokeindex = self.team.index(pokemon)
                pokeindex = self.checker.party_indices[tslot][pokeindex]
                cmd_data_bin = struct.pack(">BBB", tslot, enums.ChoiceType['SwitchType'], pokeindex)
                do_wait = False
                do_wait = all(s.battler.fainted for s in request.field.spots)
            else:
                raise NotImplementedError(command)
            data = struct.pack(">BB", 43, tslot)
            resp = (
                    ('B:0:%s:$' % tslot) + base64.b64encode(data),
                    base64.b64encode(cmd_data_bin),
                )
            self.send_command2(*resp)
            return command

    def send_command2(self, dummy, cmd):
        with ColoredStream(sys, 'stdout', (150, 150, 200)):
            print "Suppressed command:", dummy
            print "Command to send out:", cmd
            self.checker.put_line(cmd, alt_channel=True)

    @classmethod
    def load(cls, dct, *args, **kwargs):
        trainer = super(TestTrainer, cls).load(dct, *args, **kwargs)
        if dct['team'] != [m.save() for m in trainer.team]:
            for a, b in itertools.izip_longest(
                    yaml.safe_dump(dct['team'], width=29).splitlines(),
                    yaml.safe_dump([m.save() for m in trainer.team],
                        width=29).splitlines()):
                print "{0:34.34} {1:34.34}".format(a, b)
            assert False
        return trainer

class Checker(object):
    def __init__(self, battle, rules, quiet=False):
        self.battledict = battle
        self.rules = rules
        self.loader = rules.loader
        kwargs = dict(
                rand = random.Random(battle['seed']),
            )
        trainers = [TestTrainer.load(trainer_dict, checker=self,
                        monster_class=rules.MonsterClass, loader=rules.loader)
                        for trainer_dict in battle['trainers'].values()]
        self.field = rules.field(
                *trainers,
                **kwargs)

        self.last_switchout = dict()
        self.message_encoder = MessageEncoder(self)
        self.quiet = quiet
        self.party_indices = [dict((i, i) for i in range(6)) for t in range(2)]
        self.field.allow_run = False

        try:
            from regeneration import regressiontest
        except ImportError:
            pass
        else:
            args = battle['testbattle_args']
            filename = os.path.join(
                    os.path.dirname(regressiontest.__file__),
                    'data', 'results',
                    args[0],
                    '{0:03}'.format(args[1]),
                    '{0:03}.yaml'.format(args[2]))
            header = [__version__, battle['testbattle_version']] + list(args)
            if os.path.exists(filename):
                documents = yaml.load_all(open(filename, 'r'), Loader=SafeLoader)
                try:
                    file_header = documents.next()
                except StopIteration:
                    pass
                else:
                    if documents and file_header == header:
                        def checker(message):
                            print message
                            if isinstance(message, messages.BattleEnd):
                                self.winner = message.arguments['side']
                            contents = dict(message.contents())
                            del contents['field']
                            shouldbe = documents.next()
                            if contents != shouldbe:
                                with ColoredStream(sys, 'stdout', (200, 150, 150)):
                                    print yaml.safe_dump(contents)
                                with ColoredStream(sys, 'stdout', (150, 200, 150)):
                                    print yaml.safe_dump(shouldbe)
                            assert contents == shouldbe
                        self.field.add_observer(checker)
                        for trainer in trainers:
                            def cmdprint(command):
                                print 'TRAINER COMMAND', command
                                return command
                            trainer.replace_request_command(cmdprint)
                        #self.rand_parasite = RandParasite(self, self.field, battle['seed'])
                        def check(*args, **kwargs):
                            with ColoredStream(sys, 'stdout', (200, 200, 200)):
                                print args, kwargs
                        self.check = check
                        orig = self.field.flip_coin
                        def flip_coin(chance, blurb):
                            if blurb == 'Determine hit':
                                chance = Fraction(int(chance * 100), 100)
                            return orig(chance, blurb)
                        self.field.flip_coin = flip_coin
                        return
            if not os.path.exists(os.path.dirname(filename)):
                os.makedirs(os.path.dirname(filename))
            regression_file = tempfile.SpooledTemporaryFile()
            def safe_dump(dct):
                yaml.safe_dump(dct, regression_file,
                        explicit_start=True,
                        default_flow_style=True,
                        indent=0,
                        width=60,
                    )
            safe_dump(header)
            def comment(string):
                string = string.replace('\n', '\n# ')
                string = '# %s\n' % string
                regression_file.write(string)
            self.comment = comment
            def dump(message):
                comment(unicode(message).encode('utf-8'))
                dct = dict(message.contents())
                del dct['field']
                safe_dump(dct)
                if isinstance(message, messages.BattleEnd):
                    regression_file.flush()
                    regression_file.seek(0)
                    open(filename, 'w').write(regression_file.read())
                    regression_file.close()
            self.field.add_observer(dump)
            for trainer in trainers:
                def scope(trainer):
                    orig = trainer.request_command
                    def request_command_print(request):
                        command = orig(request)
                        regression_file.write('# Trainer command: %s\n' %
                                command)
                        return command
                    trainer.request_command = request_command_print
                scope(trainer)

        self.field.add_observer(self.msg_check)
        self.field.rand = None
        self.rand_parasite = RandParasite(self, self.field, battle['seed'])

        self.prepare_process()
        self.send_battledesc()

    @property
    def turn_number(self):
        return self.field.turn_number

    def gdb_backtrace(self):
        return # XXX
        try:
            self.pocheck
        except AttributeError:
            return
        if self.pocheck.returncode is None:# and sys.stdout is sys.__stdout__:
            print open('/proc/%s/stat' % self.pocheck.pid).read()
            print 'GDB:'
            gdb = subprocess.Popen(['gdb', '-batch',
                    '-ex', 'info threads',
                    '-ex', 'thread 1', '-ex', 'bt',
                    '-ex', 'thread 2', '-ex', 'bt',
                    'Server', str(self.pocheck.pid)],
                    cwd=os.path.join(po_root, 'bin'),
                    #stderr=open('/dev/null', 'w'),
                ).communicate()

    def run(self):
        with ColoredStream(sys, 'stdout', (100, 175, 175)):
            try:
                self.field.run()
            except AssertionError:
                time.sleep(0.1)
                for spot in self.field.spots:
                    battler = spot.battler
                    if battler:
                        print 'Speed', battler, battler.stats[self.loader.load_stat('speed')]
                        print 'Status', battler, battler.status
                        print 'Moves', battler, [m.name for m in battler.moves]
                        print 'Types', battler, battler.types
                        print 'Item', battler, battler.item
                if not hasattr(self, 'winner'):
                    self.gdb_backtrace()
                raise
            finally:
                print 'Effects'
                for e in self.field.active_effects:
                    try:
                        name = e.subject.name
                    except AttributeError:
                        name = type(e.subject).__name__
                    print '    %s: %s' % (name, e)
                try:
                    print 'Slave return code:', self.pocheck.returncode, self.pocheck.pid
                    self.pocheck.kill()
                except AttributeError:
                    pass
        return self.winner

    def command(self, command, response=None, ignore=False):
        self.check(command, response, ignore=ignore)

    def msg_check(self, event, response=None, ignore=False, **kwargs):
        with ColoredStream(sys, 'stdout', (200, 200, 0)):
            print '-----', event
        with ColoredStream(sys, 'stdout', (150, 150, 200)):
            self.check(event, response, ignore, **kwargs)

    def check(self, event, response=None, ignore=False, **kwargs):
        with ColoredStream(sys, 'stdout', (150, 150, 200)):
            orig_event = event
            if isinstance(event, messages.BattleStart):
                return
            elif isinstance(event, messages.Message):
                event = self.encode_message(event)
                if isinstance(event, list):
                    for e in event:
                        try:
                            e, resp = e
                        except ValueError:
                            resp = None
                        self.check(e, response=resp, ignore=ignore)
                    return
            line_string = self.do_get_line().strip()
            if line_string == event:
                print 'Event OK:', event
                with ColoredStream(sys, 'stdout', (200, 190, 150)):
                    analyze(line_string)
            elif ignore and line_string.startswith(event):
                print 'Rg line:', event, '...'
                print 'PO line:', line_string
                with ColoredStream(sys, 'stdout', (200, 190, 150)):
                    analyze(line_string)
            else:
                print type(orig_event)
                print '!!! !!! !!! !!! !!! !!!'
                print 'Rg line: ', event
                if not ignore:
                    with ColoredStream(sys, 'stdout', (200, 150, 150)):
                        analyze(event)
                print 'PO line: ', line_string
                with ColoredStream(sys, 'stdout', (150, 200, 150)):
                    analyze(line_string)
                raise AssertionError, (event, line_string)
            if response is not None:
                self.write(response)
            elif line_string.startswith('B:'):
                self.write('OK')

    def do_get_line(self):
        return self.get_line()

    def get_line(self):
        with ColoredStream(sys, 'stdout', (255, 255, 255)):
            line = self._stdout.readline()
            for delay in (0.1, 0.5, 1):
                if not line:
                    time.sleep(delay)
                    line = self._stdout.readline()
            print '<<', line.strip('\n')
            if not line:
                raise AssertionError('EOF from slave')
            return line

    def write(self, *args, **kwargs):
        alt_channel = kwargs.pop('alt_channel', False)
        o = u' '.join(unicode(arg).replace('\n', '|') for arg in args)
        self.put_line(o.encode('utf-8'), alt_channel=alt_channel)

    def put_line(self, string, alt_channel=False):
        with ColoredStream(sys, 'stdout', (250, 250, 250)):
            print ">>!" if alt_channel else ">>:", string
        if alt_channel:
            channel = self.cmd_pipe
        else:
            channel = self._stdin
        channel.write(string)
        channel.write('\n')
        channel.flush()

    def prepare_process(self, potimeout=10):
        self.poout = sys.stdout  # poout
        self.poignore = sys.stdout  # poignore
        self.cmdout = sys.stdout  # cmdout
        self.poerr = sys.stdout  # poerr
        # Create a PO process...
        olddir = os.getcwd()
        try:
            os.chdir(os.path.join(po_root, 'bin'))
            read_end, write_end = os.pipe()
            self.cmd_pipe = os.fdopen(write_end, "w")
            if self.quiet:
                kwargs = dict(stderr=open('/dev/null', 'w'))
            else:
                kwargs = dict()
            self.pocheck = pocheck = subprocess.Popen(
                    ['./Server', str(read_end)],
                    stdin=subprocess.PIPE,
                    stdout=subprocess.PIPE,
                    preexec_fn=lambda: resource.setrlimit(
                            resource.RLIMIT_CORE, (2**30, 2**30)),
                    env=dict(LD_LIBRARY_PATH='.'),
                    **kwargs)
            self._stdin = pocheck.stdin
            self._stdout = pocheck.stdout
        finally:
            os.chdir(olddir)

    def send_battledesc(self):
        for i, trainer in sorted(self.battledict['trainers'].items()):
            for j, pokemon in enumerate(trainer['team']):
                # side >> no >> speciesname >> level;
                name_index = 0
                name = (pokemon.get('nickname') or self.loader.load_form(pokemon['species']).name)
                name = name.replace(u'♀', '').replace(u'♂', '')
                full_species = pokemon['species']
                if pokemon['form']:
                    full_species = full_species, pokemon['form']
                self.command('G:Nickname', name)
                self.command('G:Species', enums.Pokemon[translateName(full_species)])
                self.command('G:Item', enums.Item[translateName(pokemon['item'])] if pokemon['item'] else 0)
                self.command('G:Ability', enums.Ability[translateName(pokemon['ability'])])
                self.command('G:Nature', enums.Nature[translateName(pokemon['nature'])])
                self.command('G:Gender', gender_table[pokemon['gender']])
                self.command('G:Shiny', int(pokemon['shiny']))
                self.command('G:Tameness', int(pokemon['tameness']))
                self.command('G:Level', pokemon['level'])
                self.command('G:Gen', 5)
                for move in (pokemon['moves'] + [None] * 4)[:4]:
                    if move is None:
                        self.command('G:Move', enums.Move['NoMove'])
                    else:
                        self.command('G:Move', enums.Move[translateName(move['kind'])])
                po_order = "hp attack defense special-attack special-defense speed".split()
                for stat_ident in po_order:
                    self.command('G:Gene', pokemon['genes'][stat_ident])
                for stat_ident in po_order:
                    self.command('G:Effort', pokemon['effort'][stat_ident])
        for i in range(2):
            self.command('C:', ignore=True)
        #for i in range(2):
        #    print i
        #    self.command('B:', ignore=True)

    def encode_message(self, message):
        return self.message_encoder.encode_message(message)


class RandParasite(object):
    def __init__(self, checker, field, seed):
        self.overrideList = []
        self.rand = random.Random(seed)
        self.checker = checker
        field.flip_coin = self.flip_coin
        field.randint = self.randint
        field.shuffle = self.shuffle

    def flip_coin(self, chance, blurb):
        print 'Coinflip blurb:', blurb, 'Chance:', chance
        chance = Fraction(chance)
        if blurb == 'Determine hit':
            chance = Fraction(int(chance * 100), 100)
        max = chance.denominator - 1
        num = chance.numerator
        randnum = self.rand.randint(0, max)
        value = randnum < num
        if not chance:
            assert value is False
            return False
        self.checker.comment(str(["R:C:%s:%s" % (num, max + 1), blurb, randnum, value]))
        self.checker.check("R:C:%s:%s" % (num, max + 1), response=value)
        return value

    def randint(self, min, max, blurb):
        print 'Randint blurb:', blurb
        value = self.rand.randint(min, max)
        self.checker.comment(str(["R:I:%s" % (max - min), blurb, value - min]))
        if min != max:
            self.checker.check("R:I:%s" % (max - min), response=value - min)
        return value

    def shuffle(self, lst, blurb):
        print 'Pre-presort:', lst
        def presort_key(item):
            #try:
                return list(self.checker.field.spots).index(item.battler.spot)
            #except (ValueError, AttributeError):
            #    pass
        print 'Shuffle blurb:', blurb
        lst.sort(key=presort_key)
        print 'Pre-shuffle:', lst
        indices = range(len(lst))
        self.rand.shuffle(indices)
        response = ' '.join(str(i) for i in indices)
        self.checker.comment(str(["R:M:%s" % len(lst), blurb, response]))
        self.checker.check("R:M:%s" % len(lst), response=response)
        lst[:] = [lst[indices[i]] for i in range(len(lst))]
        print 'Post-shuffle:', lst

def msg2both(*datastrings, **kwargs):
    players=kwargs.get('players', (0, 1))
    rv = []
    for data in datastrings:
        for player in players:
            rv.append("B:0:%s:$" % player + base64.b64encode(data))
    return rv

class MessageEncoder(object):
    def __init__(self, checker):
        self.checker = checker
        self.memory = {}

    def _slot(self, battler):
        spot = battler.spot
        sideno, side = [
                (i, s) for i, s
                in enumerate(spot.field.sides)
                if spot in s.spots
            ][0]
        spotno = [
                i for i, s
                in enumerate(side.spots)
                if s == spot
            ][0]
        return sideno + spotno * 2

    def _side(self, battler):
        spot = battler.spot
        sideno, side = [
                (i, s) for i, s
                in enumerate(spot.field.sides)
                if spot in s.spots
            ][0]
        return sideno

    def encode_message(self, message):
        battler = message.arguments.get('battler')
        opponent = message.arguments.get('opponent')
        moveeffect = message.arguments.get('moveeffect')
        move = message.arguments.get('move')
        hit = message.arguments.get('hit')
        ability = message.arguments.get('ability')
        if type(message) == messages.SendOut:
            print self.checker.party_indices
            side = self._side(battler)
            nick = battler.name.encode('utf-16-be')
            indexOfPokemon = battler.spot.trainer.team.index(battler.monster)
            rg_out = indexOfPokemon
            indexOfPokemon = self.checker.party_indices[side][indexOfPokemon]
            if hasattr(battler.monster.kind, 'species') and battler.monster.kind.species.identifier in (
                    'deoxys giratina shaymin').split():
                form_id = battler.monster.kind.species.forms.index(battler.monster.form)
            elif hasattr(battler.monster.kind, 'species') and battler.monster.kind.species.identifier in (
                    'rotom').split():
                form_id = ([None] + 'mow heat frost wash fan'.split()).index(battler.monster.form.form_identifier)
            else:
                form_id = 0
            data = struct.pack(">HHHBi",
                    self._slot(battler), indexOfPokemon,
                    battler.species.id, form_id, len(nick))
            data += nick
            gender = battler.gender.identifier
            fullstatus = dict(
                    ok=1,
                    par=2,
                    slp=4,
                    frz=8,
                    brn=16,
                )[battler.status]
            data += struct.pack(">BIBBB",
                    max(100 * battler.hp // battler.stats.hp, 1),
                    fullstatus, gender_table[gender], battler.monster.shiny,
                    battler.level)
            if side in self.checker.last_switchout:
                rg = self.checker.last_switchout[side]
                pi = self.checker.party_indices[side]
                pi[rg], pi[rg_out] = pi[rg_out], pi[rg]
            print "LAST SWITCHOUT", self.checker.last_switchout
            return msg2both(data)
        if type(message) == messages.Fainted:
            side = self._side(battler)
            data1 = struct.pack('>BBBBB',
                    enums.BattleCommand['StatusChange'],
                    side,
                    enums.Status['Koed'],
                    0, # XXX: ???
                    0, # XXX: ???
                )
            position = self._slot(battler) // 2
            data3 = struct.pack('>BB', enums.BattleCommand['Ko'], side)
            indexOfPokemon = battler.spot.trainer.team.index(battler.monster)
            self.checker.last_switchout[side] = indexOfPokemon
            return msg2both(data1)
        if isinstance(message, messages.Withdraw):
            side = self._side(battler)
            player = self._slot(battler)
            data1 = struct.pack('>BBB',
                    enums.BattleCommand['SendBack'],
                    player,
                    0, # XXX: ?
                )
            indexOfPokemon = battler.spot.trainer.team.index(battler.monster)
            self.checker.last_switchout[side] = indexOfPokemon
            return msg2both(data1)
        if type(message) == messages.TurnStart:
            with ColoredStream(sys, 'stdout', (200, 200, 0)):
                print '*' * 100
            data = struct.pack('>BbI', enums.BattleCommand['BeginTurn'], -1, message.turn)
            return msg2both(data)
        if type(message) == messages.UseMove:
            player = self._side(battler)
            self.last_used_move = moveeffect.move
            data = struct.pack('>BBHB',
                    enums.BattleCommand['UseAttack'],
                    player,
                    enums.Move[translateName(moveeffect.move.name)],
                    0, # should be 1 for forced moves
                )
            return msg2both(data)
        if isinstance(message, messages.EffectivityBase):
            player = self._side(hit.target)
            if hit.effectivity == 1:
                return []
            data = struct.pack('>BBB', enums.BattleCommand['Effective'], player, int(hit.effectivity * 4))
            return msg2both(data)
        if type(message) == messages.PPChange:
            player = self._side(battler)
            try:
                moveindex = battler.monster.moves.index(move)
                mimic = False
            except ValueError:
                moveindex = battler.moves.index(move)
                mimic = True
            pp = message.pp
            print 'PP delta:', message.delta
            if mimic:
                data = struct.pack('>BBBBB', enums.BattleCommand['ChangeTempPoke'], player, 7, moveindex, pp)
            else:
                data = struct.pack('>BBBB', enums.BattleCommand['ChangePP'], player, moveindex, pp)
            return msg2both(data, players=[player])
        if type(message) == messages.HPChange:
            player = self._side(battler)
            remaining = message.hp
            percentage = max(1, 100 * remaining // battler.stats.hp)
            if remaining <= 0:
                remaining = percentage = 0
            data1 = struct.pack('>BBH', enums.BattleCommand['ChangeHp'], player, remaining)
            data2 = struct.pack('>BBH', enums.BattleCommand['ChangeHp'], player, percentage)
            return msg2both(data1, players=(player,)) + msg2both(data2, players=(player^1,))
        if type(message) == messages.SubturnStart:
            self.memory['current_subturn'] = battler
            return []
        if type(message) == messages.SubturnEnd:
            del self.memory['current_subturn']
            return []
        if type(message) == messages.TurnEnd:
            return []
        if isinstance(message, messages.Avoid):
            player = self._slot(hit.target)
            data1 = struct.pack('>BB', enums.BattleCommand['Avoid'], player)
            return msg2both(data1)
        if type(message) == messages.Miss:
            player = self._slot(hit.user)
            data1 = struct.pack('>BB', enums.BattleCommand['Miss'], player)
            return msg2both(data1)
        if isinstance(message, messages.CriticalHit):
            player = self._slot(hit.user)
            data1 = struct.pack('>BB', enums.BattleCommand['CriticalHit'], player)
            return msg2both(data1)
        if type(message) == messages.Victory:
            side = message.arguments['side'].number
            data1 = struct.pack('>BBB', enums.BattleCommand['BattleEnd'], side, enums.BattleResult['Win'])
            data2 = struct.pack('>BBL', enums.BattleCommand['EndMessage'], side, 0xFFFFFFFF)
            data3 = struct.pack('>BBL', enums.BattleCommand['EndMessage'], side^1, 0xFFFFFFFF)
            self.checker.winner = message.arguments['side']
            return msg2both(data1, data2, data3)
        if type(message) == messages.Draw:
            data1 = struct.pack('>BBB', enums.BattleCommand['BattleEnd'], 0, enums.BattleResult['Tie'])
            self.checker.winner = None
            return []
        if type(message) == messages.DownloadActivated:
            player = self._slot(battler)
            data1 = struct.pack('>BBHBB', enums.BattleCommand['AbilityMessage'], player, 13, 0, 0)
            return msg2both(data1)
        if isinstance(message, messages.StatChange):
            player = self._slot(battler)
            delta = message.requested_delta
            stat = enums.Stat[{
                    'hp': 'HP',
                    'attack': 'Attack',
                    'defense': 'Defense',
                    'speed': 'Speed',
                    'special-attack': 'SpAttack',
                    'special-defense': 'SpDefense',
                    'accuracy': 'Accuracy',
                    'evasion': 'Evasion',
                }[message.stat.identifier]]
            if not message.delta:
                return []
            data1 = struct.pack('>BBBbB',
                    enums.BattleCommand['StatChange'],
                    player,
                    stat,
                    delta,
                    0, # XXX: ?
                )
            datas = [data1]
            return msg2both(*datas)
        if type(message) == messages.Trace:
            player = self._slot(battler)
            opponent = self._slot(opponent)
            ab = enums.Ability[translateName(message.ability.name)]
            data1 = struct.pack('>BBBBBBBBB', enums.BattleCommand['AbilityMessage'], player, 0, 66, 0, 0, opponent, 0, ab)
            return msg2both(data1)
        fixedstatus_activations = {
                messages.Paralysis.Applied: ('Paralysed', 0, None),
                messages.Burn.Applied: ('Burnt', 0, None),
                messages.Freeze.Applied: ('Frozen', 0, None),
                messages.Confusion.Applied: ('Confused', 0, None),
                messages.Freeze.Heal: ('Fine', 0, 'FreeFrozen'),
                messages.Confusion.Heal: ('Fine', 0, 'FreeConfusion'),
            }
        if type(message) in fixedstatus_activations:
            po_in, turns, free = fixedstatus_activations[type(message)]
            player = self._side(battler)
            datas = []
            if free:
                data3 = struct.pack('>BBB', enums.BattleCommand['StatusMessage'], player, enums.StatusFeeling[free])
                datas.append(data3)
            if type(message) != messages.Confusion.Heal:
                data1 = struct.pack('>BBBBB',
                        enums.BattleCommand['StatusChange'],
                        player,
                        enums.Status[po_in],
                        0, 0, # XXX: ???
                    )
                datas.append(data1)
            return msg2both(*datas)
        status_messages = {
                messages.Paralysis.PreventUse: 'PrevParalysed',
                messages.Freeze.PreventUse: 'PrevFrozen',
                messages.Burn.Hurt: 'HurtBurn',
                messages.Confusion.Tick: 'FeelConfusion',
                messages.Confusion.Hurt: 'HurtConfusion',
            }
        if type(message) in status_messages:
            po_in = status_messages[type(message)]
            player = self._side(battler)
            data1 = struct.pack('>BBB', enums.BattleCommand['StatusMessage'], player, enums.StatusFeeling[po_in])
            datas = msg2both(data1)
            if isinstance(message, messages.HPChange):
                datas += self.encode_message(messages.HPChange(**message.arguments))
            return datas
        if type(message) == messages.TwistedDimensions:
            player = self._side(battler)
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 138, 0, enums.Type['Psychic'])
            return msg2both(data1)
        if type(message) == messages.NormalDimensions:
            if 'current_subturn' in self.memory:
                player = self._side(self.memory['current_subturn'])
            else:
                player = 0
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 138, 1, enums.Type['Psychic'])
            return msg2both(data1)
        if type(message) == messages.Recoil:
            player = self._side(battler)
            data1 = struct.pack('>BB?', enums.BattleCommand['Recoil'], player, True)
            return msg2both(data1) + self.encode_message(messages.HPChange(**message.arguments))
        if type(message) == messages.NoTarget:
            player = self._side(moveeffect.user)
            data1 = struct.pack('>BB', enums.BattleCommand['NoOpponent'], player)
            return msg2both(data1)
        if type(message) == messages.ConvertedType:
            player = self._side(moveeffect.user)
            tp = message.arguments['new_type'].id - 1
            if moveeffect.move.name == 'Conversion':
                move = 19
            elif moveeffect.move.name == 'Conversion 2':
                move = 20
            data1 = struct.pack('>BBBBBBB', enums.BattleCommand['MoveMessage'], player, 0, move, 0, tp, player)
            return [
                    "B:0:0:$" + base64.b64encode(data1),
                    "B:0:1:$" + base64.b64encode(data1),
                ]
        if type(message) == messages.Failed:
            player = self._side(moveeffect.user)
            data1 = struct.pack('>BBB',
                    enums.BattleCommand['Failed'],
                    player,
                    0, # XXX: ???
                )
            return msg2both(data1)
        if type(message) == messages.ItemHeal:
            player = self._side(battler)
            args = message.arguments
            item = args.pop('item')
            if item.identifier == 'leftovers':
                msg_num = 12
            elif item.identifier == 'shell-bell':
                msg_num = 24
            else:
                msg_num = 0
            data1 = struct.pack('>BBHB', enums.BattleCommand['ItemMessage'], player, msg_num, 0)
            return msg2both(data1) + self.encode_message(messages.HPChange(**args))
        if type(message) == messages.AnnouncePressure:
            player = self._side(battler)
            data1 = struct.pack('>BBBBBB', enums.BattleCommand['AbilityMessage'], player, 0, 46, 0, 0)
            return msg2both(data1)
        if type(message) == messages.Burn.ItemApplied:
            player = self._side(battler)
            data1 = struct.pack('>BBHB', enums.BattleCommand['ItemMessage'], player, 19, 0)
            args = message.arguments
            args.pop('item')
            return msg2both(data1) + self.encode_message(messages.Burn.Applied(**args))
        if type(message) == messages.ShedSkin:
            player = self._side(battler)
            data1 = struct.pack('>BBHBBB', enums.BattleCommand['AbilityMessage'], player, 54, 0, 6, player)
            data2 = struct.pack('>BBBBB',
                    enums.BattleCommand['StatusChange'],
                    player,
                    enums.Status['Fine'],
                    0, 0, # XXX: ???
                )
            return msg2both(data1, data2)
        if type(message) == messages.Recover:
            player = self._side(battler)
            data1 = struct.pack('>BBBBBB', enums.BattleCommand['MoveMessage'], player, 0, 60, 0, enums.Type[self.last_used_move.type.name])
            args = message.arguments
            return msg2both(data1) + self.encode_message(messages.HPChange(**args))
        if type(message) == messages.Synchronize:
            player = self._side(message.arguments['synchronizer'])
            foe = self._side(battler)
            data1 = struct.pack('>BBHBBB', enums.BattleCommand['AbilityMessage'], player, 61, 0, 0, foe)
            args = message.arguments
            return msg2both(data1)
        if type(message) == messages.Sturdy:
            player = self._side(battler)
            data1 = struct.pack('>BBHBB', enums.BattleCommand['AbilityMessage'], player, 91, 0, 0)
            args = message.arguments
            return msg2both(data1)
        if type(message) == messages.MagnetRise:
            player = self._side(battler)
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 68, 0, 12)
            args = message.arguments
            return msg2both(data1)
        if type(message) == messages.MagnetRiseEnd:
            player = self._side(battler)
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 68, 1, 12)
            args = message.arguments
            return msg2both(data1)
        if type(message) == messages.AnnounceAirBalloon:
            player = self._side(battler)
            data1 = struct.pack('>BBHB', enums.BattleCommand['ItemMessage'], player, 35, 1)
            return msg2both(data1)
        if type(message) == messages.AirBalloonPopped:
            player = self._side(battler)
            data1 = struct.pack('>BBHB', enums.BattleCommand['ItemMessage'], player, 35, 0)
            return msg2both(data1)
        if type(message) == messages.TakeAim:
            player = self._side(battler)
            foe = self._side(message.arguments['target'])
            data1 = struct.pack('>BBHBBB', enums.BattleCommand['MoveMessage'], player, 74, 0, 0, foe)
            return msg2both(data1)
        if type(message) == messages.Frisk:
            player = self._side(message.arguments['frisker'])
            foe = self._side(battler)
            item = message.arguments['item']
            data1 = struct.pack('>BBHBBBH', enums.BattleCommand['AbilityMessage'], player, 23, 0, 0, foe, enums.Item[item.name])
            args = message.arguments
            return msg2both(data1)
        if type(message) == messages.NaturalCure:
            player = self._side(battler)
            data1 = struct.pack('>BBB', enums.BattleCommand['StatusChange'], player, 0)
            return msg2both(data1)
        if type(message) == messages.AbilityPreventAilment:
            player = self._side(battler)
            inducer = self._side(message.arguments['ailment'].inducer)
            data1 = struct.pack('>BBHBBBH', enums.BattleCommand['AbilityMessage'], player, 44, 1, 0, player, 20)
            return msg2both(data1)
        if type(message) == messages.Confusion.AlreadyPresent:
            player = self._side(battler)
            data1 = struct.pack('>BBB', enums.BattleCommand['AlreadyStatusMessage'], player, 6)
            return msg2both(data1)
        if type(message) == messages.ColorChange:
            player = self._side(battler)
            foe = 1 - player
            type_ = message.arguments['type']
            data1 = struct.pack('>BBHBBBH', enums.BattleCommand['AbilityMessage'], player, 9, 0, enums.Type[type_.identifier], foe, enums.Type[type_.identifier])
            return msg2both(data1)
        if type(message) == messages.AbilityBlocksHit:
            player = self._side(battler)
            if ability.identifier == 'soundproof':
                # PO rolls accuracy even when the hit is blocked!
                hit = message.arguments['hit']
                hit.move_effect.roll_accuracy(hit)
                data1 = struct.pack('>BBHBB', enums.BattleCommand['AbilityMessage'], player, 57, 0, 0)
            elif ability.identifier == 'wonder-guard':
                data1 = struct.pack('>BBHBB', enums.BattleCommand['AbilityMessage'], player, 71, 0, 0)
            return msg2both(data1)
        if type(message) == messages.ContactAilmentAbility:
            player = self._side(battler)
            subject = self._side(message.arguments['subject'])
            ability = message.arguments['ability']
            data1 = struct.pack('>BBHBBBH', enums.BattleCommand['AbilityMessage'], player, 18, 0, 17, subject, enums.Ability[ability.identifier])
            return msg2both(data1)
        return u'***{0}'.format(message)







        fixedstatus_activations = {  # ^^
                messages.ToxicActivation: ('Poisoned', 1, None),
                messages.WakeUp: ('Fine', 0, 'FreeAsleep'),
                messages.PoisonAbilityActivation: ('Poisoned', 0, '_ability'),
            }
        if type(message) in fixedstatus_activations:
            po_in, turns, free = fixedstatus_activations[type(message)]
            player = message.subject.side.number
            data1 = struct.pack('>BBB', enums.BattleCommand['StatusChange'], player, enums.Status[po_in])
            datas = [data1]
            if 'Confus' not in po_in:
                position = message.subject.number
                data2 = struct.pack('>BBBBB', enums.BattleCommand['AbsStatusChange'], player, position, enums.Status[po_in], turns)
                datas.append(data2)
            if free is '_ability':
                abi = message.ability.id
                data3 = struct.pack('>BBHBBBH', enums.BattleCommand['AbilityMessage'], player^1, 18, 0, 17, player, abi)
                datas.insert(0, data3)
            elif free:
                data3 = struct.pack('>BBB', enums.BattleCommand['StatusMessage'], player, enums.StatusFeeling[free])
                datas.append(data3)
            return msg2both(*datas)
        if type(message) == messages.ConfusionEnd:
            player = message.subject.side.number
            data = struct.pack('>BBB', enums.BattleCommand['StatusMessage'], player, enums.StatusFeeling['FreeConfusion'])
            return msg2both(data)
        status_messages = {  # ^^
                messages.PoisonHurt: 'HurtPoison',
                messages.SleepTick: 'FeelAsleep',
            }
        if type(message) in status_messages:
            po_in = status_messages[type(message)]
            player = message.subject.side.number
            data1 = struct.pack('>BBB', enums.BattleCommand['StatusMessage'], player, enums.StatusFeeling[po_in])
            return msg2both(data1)
        already_messages = {
                messages.ParalysisAlready: 'Paralysed',
                messages.FreezeAlready: 'Frozen',
                messages.BurnAlready: 'Burnt',
                messages.PoisonAlready: 'Poisoned',
            }
        if type(message) in already_messages:
            po_in = already_messages[type(message)]
            player = message.target.side.number
            data1 = struct.pack('>BBB', enums.BattleCommand['AlreadyStatusMessage'], player, enums.Status[po_in])
            return msg2both(data1)
        """
        for po_in, po_feel, po_hurt, po_heal, rg, turns in (
                ('Poisoned', 'HurtPoison', None, None, BattleMessage.StatusMessage.StatusMessage.Poison, 1),
                ('Asleep', 'FeelAsleep', None, 'FreeAsleep', BattleMessage.StatusMessage.StatusMessage.Sleep, 0),
                ('Confused', 'FeelConfusion', 'HurtConfusion', 'FreeConfusion', BattleMessage.StatusMessage.StatusMessage.Confusion, 0),
            ):
        """
        if type(message) == messages.Flinched:
            player = message.pokemon.side.number
            data1 = struct.pack('>BB', enums.BattleCommand['Flinch'], player)
            return msg2both(data1)
        if type(message) == messages.Hits:
            player = 0
            hits = message.hits
            data1 = struct.pack('>BBB', enums.BattleCommand['Hit'], player, hits)
            return  msg2both(data1)
        if type(message) == messages.LockOn:
            player = message.target.side.number
            target = message.pokemon.side.number
            data1 = struct.pack('>BBBBBBB', enums.BattleCommand['MoveMessage'], player, 0, 74, 0, 0, target)
            return msg2both(data1)
        if type(message) == messages.DonMagicCoat:
            player = message.pokemon.side.number
            data1 = struct.pack('>BBBBBB', enums.BattleCommand['MoveMessage'], player, 0, 76, 0, enums.Type['Psychic'])
            return msg2both(data1)
        if type(message) == messages.Recovered:
            player = message.pokemon.side.number
            tp = message.move.type.id - 1
            data1 = struct.pack('>BBBBBB', enums.BattleCommand['MoveMessage'], player, 0, 60, 0, tp)
            return msg2both(data1)
        if type(message) == messages.ShedSkin:
            player = message.subject.side.number
            tp = enums.Type['Bug']
            data1 = struct.pack('>BBHBBB', enums.BattleCommand['AbilityMessage'], player, 54, 0, tp, player)
            data2 = struct.pack('>BBB', enums.BattleCommand['StatusChange'], player, enums.Status['Fine'])
            position = message.subject.number
            data3 = struct.pack('>BBBBB', enums.BattleCommand['AbsStatusChange'], player, position, enums.Status['Fine'], 0)
            return msg2both(data1, data2, data3)
        if type(message) in (messages.RestActivation, messages.HealRestActivation):
            player = message.subject.side.number
            tp = enums.Type['Psychic']
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 106, 0, tp)
            data2 = struct.pack('>BBB', enums.BattleCommand['StatusChange'], player, enums.Status['Asleep'])
            position = message.subject.number
            data3 = struct.pack('>BBBBB', enums.BattleCommand['AbsStatusChange'], player, position, enums.Status['Asleep'], 0)
            return msg2both(data1, data3) + [('RANDINT', 0)]
        if type(message) == messages.MadeSubstitute:
            player = message.subject.side.number
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 128, 4, 0)
            data2 = struct.pack('>BBB', enums.BattleCommand['Substitute'], player, 1)
            return msg2both(data1, data2)
        if type(message) == messages.SubTookHit:
            player = message.subject.side.number
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 128, 3, 0)
            return msg2both(data1)
        if type(message) == messages.SubstituteBroken:
            player = message.subject.side.number
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 128, 1, 0)
            data2 = struct.pack('>BBB', enums.BattleCommand['Substitute'], player, 0)
            return msg2both(data1, data2)
        if type(message) == messages.EndureBrace:
            player = message.subject.side.number
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 35, 1, 0)
            return msg2both(data1)
        if type(message) == messages.Endured:
            # XXX: PO completely ignores this message
            return []
        if type(message) == messages.Synchronize:
            player = message.inducer.side.number
            target = message.subject.side.number
            effname = str(message.effect.msg_name.__doc__)
            effname=dict(Paralysis='Paralysed', Burn='Burnt', Poison='Poisoned').get(effname, effname)
            data1 = struct.pack('>BBHBBB', enums.BattleCommand['AbilityMessage'], player, 61, 0, 0, player^1)
            data2 = struct.pack('>BBB', enums.BattleCommand['StatusChange'], target, enums.Status[effname])
            position = message.subject.number
            data3 = struct.pack('>BBBBB', enums.BattleCommand['AbsStatusChange'], target, position, enums.Status[effname], 0)
            return msg2both(data1, data2, data3)
        if type(message) == messages.Sturdy:
            player = message.pokemon.side.number
            data1 = struct.pack('>BBBBBB', enums.BattleCommand['AbilityMessage'], player, 0, 91, 0, 0)
            return msg2both(data1)
        if type(message) == messages.SunActivation:
            player = message.inducer.side.number
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 57, 3, 9)
            return msg2both(data1)
        if type(message) == messages.RainActivation:
            player = message.inducer.side.number
            data1 = struct.pack('>BBHBB', enums.BattleCommand['MoveMessage'], player, 57, 1, 10)
            return msg2both(data1)
        weather_msgs = {
                messages.SunTick: (0, 4),
                messages.RainTick: (0, 2),
                messages.SunEnd: (1, 4),
                messages.RainEnd: (1, 2),
            }
        if type(message) in weather_msgs:
            a, b = weather_msgs[type(message)]
            player = message.inducer.side.number
            data1 = struct.pack('>BBBB', enums.BattleCommand['WeatherMessage'], 0, a, b)
            return msg2both(data1)
        if type(message) == messages.Mimic:
            player = message.pokemon.side.number
            source = message.source.side.number
            move = message.move
            slot = list(i for i,m in enumerate(message.pokemon.pokemon.moves) if m is move)[0]
            moveid = move.movetype.id
            data1 = struct.pack('>BBBBH', enums.BattleCommand['ChangeTempPoke'], player, 0, slot, moveid)
            data2 = struct.pack('>BBBBB', enums.BattleCommand['ChangeTempPoke'], player, 7, slot, 5)
            data3 = struct.pack('>BBHBBBH', enums.BattleCommand['MoveMessage'], player, 81, 0, 0, source, moveid)
            return msg2both(data1, data2, players=[player]) + msg2both(data3)
        if type(message) == messages.MagicCoatReflect:
            player = message.pokemon.side.number
            move = message.move
            coat = message.coat_move
            data1 = struct.pack('>BBHBBBH', enums.BattleCommand['MoveMessage'], player, 76, 1, 13, player, move.movetype.id)
            return msg2both(data1)
        return

    def formatMessage2(self, message):
        if message.langs.get('x-po', True) is None:
            return None
        return 'MESSAGE  ' + MessageFormatter.formatMessage(self, message)

    def formatPokemon(self, pokemon):
        for i, side in enumerate(pokemon.field.sides):
            for j, pk in enumerate(side.spots[0].trainer.team):
                if pk == pokemon.pokemon:
                    return "@p{%d,%d}" % (i, j)
        return "@p{???}"

    def formatMove(self, move):
        return translateName(MessageFormatter.formatMove(self, move))

    def formatStat(self, stat):
        d = dict((a, b) for (b, a) in enumerate("""
                HP Attack Defense Speed SpecialAttack SpecialDefense Accuracy
                Evasion
            """.split()))
        #return '${3,%d}' % d[stat.name.replace(' ', '')]  # XXX: remove if unneeded
        return '@{3,%d}' % stat.number

    def formatType(self, type):
        return '@{0,%s}' % type.name


    def formatItem(self, item):
        return translateName(item.name)

    def formatStatusEffect(self, effect):
        from regeneration.battle import effects
        if isinstance(effect, effects.Confusion):
            return '@{7,0}'
        elif isinstance(effect, effects.Freeze):
            return '@{8,0}'
        elif isinstance(effect, effects.Paralysis):
            return '@{9,0}'
        elif isinstance(effect, effects.Poison):
            return '@{10,0}'
        elif isinstance(effect, effects.Sleep):
            return '@{11,0}'
        else:
            return '@{????,0}'

def real_main(argv):
    if argv is None:
        argv = sys.argv
    if not argv[1:]:
        from regeneration.poxcheck.runner import main
        main()
    args = dict(enumerate(argv))
    impl = int(args.get(1, -1))
    seed = int(args.get(2, 0))
    battle_format = testbattle.literal_eval(args.get(3, '[[0], [1]]'))
    b, r = testbattle.battle_and_rules(impl, seed)
    print testbattle.yaml.safe_dump(b)
    print Checker(b, r).run()

def main(argv=None):
    import cProfile
    if argv is None:
        argv = sys.argv
    cProfile.runctx("real_main(argv)",
             globals(), locals(), filename='poxcheck.profile')

if __name__ == '__main__':
    main()
