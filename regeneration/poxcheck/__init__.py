
__copyright__ = 'Copyright 2011, Petr Viktorin'
__license__ = 'MIT'
__email__ = 'encukou@gmail.com'

# Specify the PO repo root at the following location
from regeneration.poxcheck.config import po_root
